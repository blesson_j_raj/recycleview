package com.example.blessonjraj.recycleviewlisting.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.blessonjraj.recycleviewlisting.R;
import com.example.blessonjraj.recycleviewlisting.model.UserData;

public class DetailActivity extends AppCompatActivity {

     UserData userData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        userData = (UserData) getIntent().getSerializableExtra("userData");

    }
}
