package com.example.blessonjraj.recycleviewlisting.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.blessonjraj.recycleviewlisting.R;
import com.example.blessonjraj.recycleviewlisting.adaptor.RecycleViewAdpator;
import com.example.blessonjraj.recycleviewlisting.model.UserData;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

 RecyclerView recyclerView;
 List<UserData> userData;
    RecycleViewAdpator recycleViewAdpator;
   UserData userDataMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userData = new ArrayList<>();
    recyclerView = findViewById(R.id.userRecyclerView);
        userDataMain=new UserData();
        fetchdata();
    }

 private void fetchdata(){
   String mJSONURLString = "https://jsonplaceholder.typicode.com/users";
     RequestQueue requestQueue = Volley.newRequestQueue(this);

     // Initialize a new JsonArrayRequest instance
     JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
             Request.Method.GET,
             mJSONURLString,
             null,
             new Response.Listener<JSONArray>() {
                 @Override
                 public void onResponse(JSONArray response) {

                     Gson gson = new Gson();
                     userData = Arrays.asList(gson.fromJson(response.toString(),UserData[].class));


                     Toast.makeText(MainActivity.this, "ok", Toast.LENGTH_SHORT).show();
                      recycleViewAdpator = new RecycleViewAdpator( getApplicationContext(),userData, new RecycleViewAdpator.UserPackageListingItemClickListner() {
                         @Override
                         public void onClick(UserData userData) {
                             Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                             intent.putExtra("userData", userData);
                             startActivity(intent);
                         }
                     });

                     LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                     recyclerView.setLayoutManager(linearLayoutManager);
                     recyclerView.setAdapter(recycleViewAdpator);

//
//                     for (UserData userData:userData){
//                         Toast.makeText(MainActivity.this, userData.getEmail(), Toast.LENGTH_SHORT).show();
//                     }
                 }
             },
             new Response.ErrorListener(){
                 @Override
                 public void onErrorResponse(VolleyError error){
                     // Do something when error occurred

                 }
             }
     );

     // Add JsonArrayRequest to the RequestQueue
     requestQueue.add(jsonArrayRequest);


 }
}
