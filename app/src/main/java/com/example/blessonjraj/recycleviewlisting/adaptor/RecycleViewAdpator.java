package com.example.blessonjraj.recycleviewlisting.adaptor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blessonjraj.recycleviewlisting.R;
import com.example.blessonjraj.recycleviewlisting.model.Address;
import com.example.blessonjraj.recycleviewlisting.model.UserData;

import java.util.List;

public class RecycleViewAdpator extends RecyclerView.Adapter<RecycleViewAdpator.RecycleViewHolder> {
    View qwertyview;
    List<UserData>userData;
    Context context ;
    UserPackageListingItemClickListner userPackageListingItemClickListner;

    public RecycleViewAdpator( Context context,List<UserData> userData, UserPackageListingItemClickListner userPackageListingItemClickListner) {
        this.userData = userData;
        this.context = context;
        this.userPackageListingItemClickListner = userPackageListingItemClickListner;
    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        qwertyview = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_layout,viewGroup,false);
        return new RecycleViewHolder(qwertyview);
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder recycleViewHolder, int i) {
              recycleViewHolder.setData(userData.get(i));
    }

    @Override
    public int getItemCount() {
        return userData.size();
    }


    public class RecycleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvname ,tvemail;

        UserData userData;

        public RecycleViewHolder(@NonNull View itemView) {
            super(itemView);
              tvname = itemView.findViewById(R.id.textView);
              tvemail = itemView.findViewById(R.id.textView2);
            itemView.setOnClickListener(this);


        }

        public void setData(UserData userData) {
               this.userData = userData ;
               tvname.setText(userData.getName());
               tvemail.setText(userData.getUsername());
        }

        @Override
        public void onClick(View v) {
            if (userPackageListingItemClickListner != null){
                userPackageListingItemClickListner.onClick(userData);

            }
        }
    }
    public interface UserPackageListingItemClickListner{

         void onClick(UserData userData);
    }

}
