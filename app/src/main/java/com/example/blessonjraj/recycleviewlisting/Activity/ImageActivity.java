package com.example.blessonjraj.recycleviewlisting.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.blessonjraj.recycleviewlisting.R;
import com.example.blessonjraj.recycleviewlisting.adaptor.ImageRecycleView;
import com.example.blessonjraj.recycleviewlisting.adaptor.RecycleViewAdpator;
import com.example.blessonjraj.recycleviewlisting.model.ImageList;
import com.example.blessonjraj.recycleviewlisting.model.UserData;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ImageActivity extends AppCompatActivity {

    List<ImageList> imageLists;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        imageLists = new ArrayList<>();
        recyclerView = findViewById(R.id.imageList);
        fetchdata();
    }


    private void fetchdata(){
        String mJSONURLString = "https://jsonplaceholder.typicode.com/photos";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        // Initialize a new JsonArrayRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                mJSONURLString,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        Gson gson = new Gson();

                        imageLists =Arrays.asList(gson.fromJson(response.toString(),ImageList[].class));

                        ImageRecycleView imageRecycleView = new ImageRecycleView(getApplicationContext(), imageLists, new ImageRecycleView.imageClickClickListner() {
                            @Override
                            public void Onclick(ImageList imageList) {

                            }
                        });

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),2);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(imageRecycleView);



                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        // Do something when error occurred

                    }
                }
        );

        // Add JsonArrayRequest to the RequestQueue
        requestQueue.add(jsonArrayRequest);


    }

}
