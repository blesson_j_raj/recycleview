package com.example.blessonjraj.recycleviewlisting.adaptor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.blessonjraj.recycleviewlisting.R;
import com.example.blessonjraj.recycleviewlisting.model.ImageList;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageRecycleView extends RecyclerView.Adapter<ImageRecycleView.ImageViewHolder> {

    Context context;

    List<ImageList>imageLists;
    imageClickClickListner imageClickClickListner;

    public ImageRecycleView(Context context, List<ImageList> imageLists, ImageRecycleView.imageClickClickListner imageClickClickListner) {
        this.context = context;
        this.imageLists = imageLists;
        this.imageClickClickListner = imageClickClickListner;
    }


    @NonNull
    @Override
    public ImageRecycleView.ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_layout_image_list,viewGroup,false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageRecycleView.ImageViewHolder imageViewHolder, int i) {
                 imageViewHolder.setData(imageLists.get(i));
    }

    @Override
    public int getItemCount() {
        return imageLists.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageList imageList;
         ImageView imageView ;
         TextView textView ;
        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
        }

        public void setData(ImageList imageList) {
            this.imageList = imageList;
            Picasso.get().load(imageList.getThumbnailUrl()).into(imageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (imageClickClickListner != null){
                imageClickClickListner.Onclick(imageList);
            }

        }
    }

    public interface imageClickClickListner{
              void Onclick(ImageList imageList);
    }
}
